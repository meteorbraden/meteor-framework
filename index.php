<?php
	
// (1) Let the meteorness begin!
//     Include meteor SETUP
// ----------------------------
	if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/meteor/framework/setup.php'))
		include($_SERVER['DOCUMENT_ROOT'] . '/meteor/framework/setup.php');
	
	

// (2) Route the request
// ---------------------

	$uri_parts = explode('/', METEOR_URI);
	
	// (2.1) IF Requesting Admin
	// -------------------------
	if(isset($uri_parts[0]) && $uri_parts[0] == METEOR_ADMIN_ID)
	{
		if(file_exists(METEOR_FRAMEWORK_PATH . '/route_to_admin.php'))
			include(METEOR_FRAMEWORK_PATH . '/route_to_admin.php');
		else
			echo 'Admin controller not found.';
	}
	
	// (2.1) ELSE IF Requesting login
	// ------------------------------
	else if(isset($uri_parts[0]) && $uri_parts[0] == METEOR_LOGIN_ID)
	{
		if(file_exists(METEOR_FRAMEWORK_PATH . '/login.php'))
			include(METEOR_FRAMEWORK_PATH . '/login.php');
		else
			echo 'Admin login not found.';
	}
	
	// (2.2) ELSE Requesting Content
	// -----------------------------
	else
	{
		if(file_exists(METEOR_FRAMEWORK_PATH . '/route_to_content.php'))
			include(METEOR_FRAMEWORK_PATH . '/route_to_content.php');
		else
			echo 'Content controller not found.';
	}




/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* /// Functions ////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */

	



?>