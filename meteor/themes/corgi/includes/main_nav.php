                        <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                            <ul class="nav navbar-nav c-theme-nav">
                                <li class="c-active">
                                    <a href="/" class="c-link">Home
                                        <span class="c-arrow"></span>
                                    </a>
                                    <div class="c-menu-type-mega c-menu-type-fullwidth" style="min-width: auto">
               
                                            <div class="col-md-3">

                                <li class="c-menu-type-classic">
                                    <a href="/about" class="c-link">About
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>                       
                                <li>
                                    <a href="/blog" class="c-link">Blog
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/shop" class="c-link">Shop
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/faq" class="c-link">FAQ
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/forums" class="c-link">Forums
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                 <li>
                                    <a href="https://www.facebook.com/corgicommunity" target="_blank" class="c-link"><i class="fa fa-facebook fa-db"></i>
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                 <li>
                                    <a href="https://twitter.com/corgi_community" target="_blank" class="c-link"><i class="fa fa-twitter fa-db"></i>
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                 <li>
                                    <a href="https://www.instagram.com/thecorgicommunity" target="_blank" class="c-link"><i class="fa fa-instagram fa-db"></i>
                                        <span class="c-arrow c-toggler"></span>
                                    </a>
                                </li>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                        </nav>