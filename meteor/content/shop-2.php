

<?php 
    
    // Set Meta Data 
    // --------------------------------    
    
    {
    
    
    include($server_path . '/' . $sff_dir . '/functions.php');
    $products = get_products();
    
        if(isset($_GET['product']))
    {
        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));
    
            $productname = $product[0]['title'];
            $productid = $product[0]['id'];
    
     
    if ($productid > 1) { 
        
        $productid = $productname;     
    }  
    
    }
    
    else {
        $productname = 'products';
    }


?>

<!--
    @template = tier
    @title = <?php echo $productname; ?> 
    @keywords =
    @description =
-->


    

<?php }

    // Get products 
    // --------------------------------
    

    
    if(isset($_GET['product']))
    {
        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));


        $num_products = count($products);


?>



	<!-- BEGIN: PAGE CONTAINER -->
        <div class="c-layout-page">
            <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
            <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
                <div class="container">
                    <div class="c-page-title c-pull-left">
                        <h3 class="c-font-uppercase c-font-sbold"><?php echo $product[0]['title']; ?></h3>

                    </div>
                    <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                        <li>
                            <a href="/shop">Shop</a>
                        </li>
                        <li>/</li>
                        <li class="c-state_active"><?php echo $product[0]['title']; ?></li>
                    </ul>
                </div>
            </div>
            <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
            <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
                <div class="container">
                    <div class="c-shop-product-details-2">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="c-product-gallery">
                                    <div class="c-product-gallery-content">
                                        <div class="c-zoom">
                                            <img src="<?php echo $product[0]['image']; ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="c-product-meta">
                                    <div class="c-content-title-1">
                                        <h3 class="c-font-uppercase c-font-bold"><?php echo $product[0]['title']; ?></h3>
                                        <div class="c-line-left"></div>
                                    </div>
                                    <div class="c-product-badge">
                                        <div class="c-product-new">New</div>
                                    </div>
                                    <div class="c-product-review">
                                        <div class="c-product-rating">
                                            <i class="fa fa-star c-font-red"></i>
                                            <i class="fa fa-star c-font-red"></i>
                                            <i class="fa fa-star c-font-red"></i>
                                            <i class="fa fa-star c-font-red"></i>
                                            <i class="fa fa-star-half-o c-font-red"></i>
                                        </div>
                                    </div>
                                    <div class="c-product-price"><?php echo $product[0]['price']; ?></div>
                                    <div class="c-product-short-desc"><?php echo $product[0]['description']; ?></div>
                                            <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                                <a href="<?php echo $product[0]['link']; ?>" target="_blank"><button class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase">Buy Now</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
        </div>
        

            <!-- product Profile End -->
        
        <?php }
    
    
        
    // All products 
    // --------------------------------
    else
    {

        ?>
        
                    
            <!-- BEGIN: PAGE CONTAINER -->
        <div class="c-layout-page">
            <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
            <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
                <div class="container">
                    <div class="c-page-title c-pull-left">
                        <h3 class="c-font-uppercase c-font-sbold">Shop</h3>
                    </div>
                    <h4 class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">Corgi Apparel and Accessories</h4>
                </div>
            </div>
             <div class="c-layout-sidebar-content ">
 
                    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
                    <div class="c-bs-grid-small-space">
                        <div class="row">
	                        
	                             <?php
						            
						            foreach($products as $i => $v)
												{
													
													?>

	                        <!-- Products -->
                            <div class="col-md-3 col-sm-6 c-margin-b-20">
                                <div class="c-content-product-2 c-bg-white c-border">
                                    <div class="c-content-overlay">
                                        <div class="c-overlay-wrapper">
                                            <div class="c-overlay-content">
                                                <a href="<?php echo $v['link']; ?>" target="_blank" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Buy Now</a>
                                            </div>
                                        </div>
                                        <img src="<?php echo $v['image']; ?>" alt="Keep Calm and Corgi On Shirt" style="width: 100%;" />
                                    </div>
                                    <div class="c-info">
                                        <p class="c-title c-font-16 c-font-slim"><?php echo $v['title']; ?></p>
                                        <p class="c-price c-font-14 c-font-slim"><?php echo $v['price']; ?></p>
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group c-border-top" role="group">
                                            <a href="?product=<?php echo $v['alias']; ?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">View More</a>
                                        </div>
                                        <div class="btn-group c-border-left c-border-top" role="group">
                                            <a href="<?php echo $v['link']; ?>" target="_blank" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Buy Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php } ?>                            
                            
                            
                            
						</div>
                    </div>
                 </div>
            </div>
        </div>
        <!-- END: PAGE CONTAINER -->

    <?php } ?>                    
                
                        






