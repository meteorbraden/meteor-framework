<?php

	// (1) Setup
	// ---------
	include_module('shop');
	$message = '';
	
	
	// (1) Check for delete
	// --------------------
	if(isset($_GET['delete']) && is_numeric($_GET['delete']))
	{
		$query = "DELETE FROM products WHERE id IN ('" . $db->real_escape_string($_GET['delete']) . "')";
		$success = $db->query($query) or die('Error in the consult..' . mysqli_error($db));
		
		if($success)
			$message = '<div class="alert alert-success" role="alert">Whoa, that product is GONE!</div>';
		else
			$message = '<div class="alert alert-danger" role="alert">Well that didn\'t work</div>';
	}
	
	$products = products_get_all();
	echo $message;
		
?>

<table class="table table-striped">
	
	<thead>
		<tr>
			<th>Name</th>
			<th style="width: 20%; text-align: center;">Image</th>
		</tr>
	</thead>
	
	<?php foreach($products as $product) { ?>
	<tr>
		<td><a href=""><?php echo $product['title']; ?></a></td>
		<td><a href=""><?php echo $product['alias']; ?></a></td>
		<td><a href=""><?php echo $product['link']; ?></a></td>
		<td><a href=""><?php echo $product['image']; ?></a></td>
		<td><a href=""><?php echo $product['price']; ?></a></td>
		<td><a href=""><?php echo $product['description']; ?></a></td>
		<td style="text-align: center;"><a href=""><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> <a href="?delete=<?php echo $product['id']; ?>" style="color: maroon;"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a></td>
	</tr>
	<?php } ?>
	
</table>