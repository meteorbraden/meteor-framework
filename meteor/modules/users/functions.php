<?php


// Relocate and error handling
// ---------------------------
	if(basename($_SERVER['PHP_SELF']) == 'functions.php') { header('Location: http://' . $_SERVER['HTTP_HOST']); exit(); }


// (x) Check login
// ---------------
	
	function users_login($username, $password)
	{
		global $db;
		
		$query = "SELECT user.id, user.username FROM users AS user
					WHERE 
						user.username = '" . $db->real_escape_string($username) . "'
						AND
						user.password = '" . $db->real_escape_string($password) . "'";
					
		$user = data_get_array($query);
		
		if(isset($user[0]['id']))
		{
			unset($_SESSION['meteor']['user']);
			$_SESSION['meteor']['user'] = $user[0];
			
			return true;
		}
		else
			return false;
	}



// (x) Get all users
// -----------------
	
	function users_get_all()
	{
		global $db;
		
		$query = "SELECT users.* FROM users";
		return data_get_array($query);	
	}
	
	
	
// (x) Format username
// -------------------
	
	function users_format_username($username)
	{
		return strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $username));
	}
	
	
	
// (x) Hash password
// -----------------
	
	function users_hash($password)
	{
		return sha1($password);
	}
	
	
?>