<?php

	// (1) Setup
	// ---------
	include_module('users');
	$message = '';
	
	
	// (1) Check for delete
	// --------------------
	if(isset($_GET['delete']) && is_numeric($_GET['delete']))
	{
		$sql = "DELETE FROM users WHERE id IN ('" . $db->real_escape_string($_GET['delete']) . "')";
		$success = data_query($sql);
		
		if($success)
			$message = '<div class="alert alert-success" role="alert">Whoa, that dude is GONE!</div>';
		else
			$message = '<div class="alert alert-danger" role="alert">Well that didn\'t work</div>';
	}
	
	$users = users_get_all();
	echo $message;
	
?>
<table class="table table-striped">
	
	<thead>
		<tr>
			<th>User</th>
			<th style="width: 20%; text-align: center;">Actions</th>
		</tr>
	</thead>
	
	<?php foreach($users as $user) { ?>
	<tr>
		<td><a href=""><?php echo $user['username']; ?></a></td>
		<td style="text-align: center;"><a href=""><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> <a href="?delete=<?php echo $user['id']; ?>" style="color: maroon;"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a></td>
	</tr>
	<?php } ?>
	
</table>