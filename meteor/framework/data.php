<?php

// Relocate and error handling
// ---------------------------
	if(basename($_SERVER['PHP_SELF']) == 'settings.php') { header('Location: http://' . $_SERVER['HTTP_HOST']); exit(); }

	
	// (1) Connect to database
	// -----------------------
	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Error ' . mysqli_error($db));	
	
	
	// (2) DATA FUNCTIONS
	/* //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */
	
	
	
		// (x) Get site settings
		// ---------------------
		function get_site_settings()
		{
			$query = 'SELECT settings.* FROM settings WHERE 1';
			return data_get_array($query);
		}
		
		
		// (x) Query
		// ---------
		function data_query($sql)
		{
			global $db;
			
			return $db->query($sql) or die('Error in the consult..' . mysqli_error($db));
		}

		
		// (x) Get site setting from SESSION
		// --------------------------------
		function data_get_setting($get)
		{
			$r = '';
			if(isset($_SESSION['meteor']['site_settings']) && !empty($_SESSION['meteor']['site_settings']))
			{
				foreach($_SESSION['meteor']['site_settings'] as $i => $v)
					if($i == $get)
						$r = $v;
			}
			return $r;
		}
		
			
		// (x) Run Query and return as array
		// ---------------------------------
		function data_get_array($query)
		{
			global $db;
			
			$result = $db->query($query) or die('Error in the consult..' . mysqli_error($db));
			$return = array();
			
			while($row = $result->fetch_assoc())
				$return[] = $row;
	
			return $return;
		}
		
		
		
		
	// (2) FORMATTING FUNCTIONS
	/* //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */
		
		// (x) Run Query and return as array
		// ---------------------------------
		function data_secure_implode($array)
		{
			global $db;
			$r = $names = $values = array();
			
			foreach($array as $i => $v)
				$names[$i] = $db->real_escape_string(preg_replace("/[^a-z]_+/", "", $i));
			
			foreach($array as $i => $v)
				$values[$i] = $db->real_escape_string($v);
			
			$r['names'] = implode(",", $names);
			$r['values'] = implode("','", $values);
			
			return $r;
		}
	
?>