<?php
	
	
// (1) Setup
// ---------

	$theme = data_get_setting('theme');
	$theme_path = METEOR_THEME_PATH . '/' . $theme;
	$theme_web_path = METEOR_THEME_WEB_PATH . '/' . $theme;
	$module_web_path = METEOR_MODULE_WEB_PATH;
	
	
// (2) Get content
// ---------------

	$allowed_pages = array('php', 'html');
	$found_page = $found_error_page = false;
	$content = 'Conditional content not found.';
	$page = METEOR_URI == '' ? data_get_setting('home_content') : str_replace("/", METEOR_DEPTH_ID, METEOR_URI);
	
	foreach($allowed_pages as $index => $value)
	{
		$tmp = METEOR_CONTENT_PATH . '/' . $page . '.' . $value;
		if(file_exists($tmp) && is_readable($tmp))
		{
			$found_page = true;
			break;
		}
	}
		
		// IF page content found
		if($found_page)
		{
		    ob_start();
		    include($tmp);
		    $content = ob_get_clean();
		}
		
		// ELSE
		else
			$content = error_content();
	
	
	// Tag info
	$page_data = tag_information($content);


	
// (3) Get template
// ----------------

	$load_template = isset($page_data['template']) && trim($page_data['template']) != '' ? trim($page_data['template']) : data_get_setting('default_template');
	$tmp = $theme_path . '/' . $load_template . '.php';
	
	$template = 'Template Not Found (' . $tmp . ')';
	if(file_exists($tmp) && is_readable($tmp))
	{
		ob_start();
		include($tmp);
		$template = ob_get_clean();
	}


	
// (4) Set template vars
// ---------------------
	
	// Constant vars
	$template_vars = array(
						'title' => data_get_setting('default_title'),
						'keywords' => data_get_setting('default_keywords'),
						'description' => data_get_setting('default_description'),
						'site_name' => data_get_setting('site_name'),
						'theme_path' => $theme_web_path,
						'module_path' => $module_web_path
						);
						
	// Tag overrides
	$template_vars = array_merge($template_vars, $page_data);

	
	
// (5) Load and echo website
// -------------------------
	
	$website = $template;
	
	/* RUN TWICE AROUND INCLUDES: FIRST */
		$website = str_replace('{content}', $content, $website);
		
		foreach($template_vars as $i => $v)
			$website = str_replace('{' . $i . '}', $v, $website);
		
		if(isset($_SESSION['meteor']['site_settings']) && !empty($_SESSION['meteor']['site_settings']))
			foreach($_SESSION['meteor']['site_settings'] as $i => $v)
				$website = str_replace('{' . $i . '}', $v, $website);
	
	
	// Insert includes
	while(strstr($website, "{include:"))
	{	
		preg_match("/\{include:.*\}/Uis", $website, $match);
		$include = str_replace('{include:', '', $match[0]);
		$include = str_replace('}', '', $include);
		
		$f = $theme_path . '/includes/' . $include . '.php';
		$r = 'Include file not found (' . $theme . '/includes/' . $include . '.php)';
		
		if(file_exists($f))
		{
			ob_start();
			include($f);
			$r = ob_get_clean();
		}
		
		$website = str_replace($match[0], $r, $website);
	}
	
	
	/* RUN TWICE AROUND INCLUDES: SECOND */
		$website = str_replace('{content}', $content, $website);
		
		foreach($template_vars as $i => $v)
			$website = str_replace('{' . $i . '}', $v, $website);
			
		if(isset($_SESSION['meteor']['site_settings']) && !empty($_SESSION['meteor']['site_settings']))
			foreach($_SESSION['meteor']['site_settings'] as $i => $v)
				$website = str_replace('{' . $i . '}', $v, $website);
	
	
	// Echo Website
	// -----------
	echo $website;
	
	
	
	
	
	
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* /// Functions ////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */

	
	// Get error page content and set headers
	// --------------------------------------
	function error_content()
	{
		global $allowed_pages, $found_error_page;
		
		header('HTTP/1.0 404 Not Found');
		foreach($allowed_pages as $index => $value)
		{
			$tmp404 = METEOR_CONTENT_PATH . '/404.' . $value;
			if(file_exists($tmp404) && is_readable($tmp404))
			{
				$found_error_page = true;
				break;
			}
		}
		
		if($found_error_page)
		{
			ob_start();
			include($tmp404);
			return ob_get_clean();
		}
		else
			return 'Page not found.';
	}
	
	
	// Return page information
	// -----------------------
	function tag_information($string)
	{
		$item = array('template', 'title', 'keywords', 'description', 'add_to_head');
		$array = array();
		foreach($item as $value)
		{
			$r = array();
			$tag = $value;
			// grab and replace
			preg_match("/@" . $tag . " = .*\n/Uis", $string, $match);
			if(!empty($match[0]))
			{
				$r['match'] = $match[0];
				$r['block'] = trim(str_replace("@" . $tag . " = ", "", $r['match']));
				$array = array_merge($array, array($value => $r['block']));
			}
			else
				$array = array_merge($array, array($value => ''));
		}
		return $array; 
	}



?>