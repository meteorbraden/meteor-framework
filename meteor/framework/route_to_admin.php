<?php
	
	
// (1) Authorize
// -------------
	
	if(!isset($_SESSION['meteor']['user']))
	{
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . METEOR_LOGIN_ID);
		exit();
	}
	
	
	
// (2) Setup
// ---------

	$theme_path = METEOR_THEME_PATH . '/admin';
	$theme_web_path = METEOR_THEME_WEB_PATH . '/admin';
	$modules = array_diff(scandir(METEOR_MODULE_PATH), explode(",", METEOR_INVISIBLE_FILES));
	$content = array_diff(scandir(METEOR_CONTENT_PATH), explode(",", METEOR_INVISIBLE_FILES));
	$module_nav = $controller_nav = array();
	


// (3) Get module
// --------------

	$current_module = $module_name = $default_controller = $module_nav_html = '';
	if(isset($uri_parts[1]) && $uri_parts[1] != '')
	{
		$current_module = $uri_parts[1];
		$module_name = ucwords(str_replace('_', ' ', $current_module));
		
		if(file_exists(METEOR_MODULE_PATH . '/' . $current_module . '/admin.php'))
			include(METEOR_MODULE_PATH . '/' . $current_module . '/admin.php');
		else
			echo 'Module admin file not found';
	}


	
// (4) Get controller
// ------------------

	$current_controller = $controller_name = $controller_nav_html = $controller = '';
	if((isset($uri_parts[2]) && $uri_parts[2] != '') || $default_controller != '')
	{
		$current_controller = (isset($uri_parts[2]) && $uri_parts[2] != '') ? $uri_parts[2] : $default_controller;
		$controller_name = ucwords(str_replace('_', ' ', $current_controller));
		
		if(file_exists(METEOR_MODULE_PATH . '/' . $current_module . '/' . $current_controller . '.php'))
		{
			ob_start();
			include(METEOR_MODULE_PATH . '/' . $current_module . '/' . $current_controller . '.php');
			$controller = ob_get_clean();
		}
		else
			echo 'Module controller file not found';
	}
	
	// Set parts for parent checking etc.
	$controller_parts = explode('.', $current_controller);
	

// (5) Controller nav
// ------------------

	if(!empty($controller_nav[$controller_parts[0]]))
	{
		$controller_nav_html = '<ul class="nav nav-pills nav-stacked">';
		
		foreach((array)$controller_nav[$controller_parts[0]] as $i => $v)
		{
			$cur = ($current_controller == $i) ? ' class="active"' : '';
			$controller_nav_html .= '<li' . $cur . '><a href="/' . METEOR_ADMIN_ID . '/' . $current_module . '/' . $i . '"><span>' . $v . ' </span></a></li>';
		}
		
		$controller_nav_html .= '</ul>';
	}



// (6) Module nav
// --------------
	if(!empty($module_nav))
	{
		$module_nav_html = '';
		
		foreach((array)$module_nav as $i => $v)
		{
			$cur = ($controller_parts[0] == $i) ? ' class="active"' : '';
			$module_nav_html .= '<li' . $cur . '><a href="/' . METEOR_ADMIN_ID . '/' . $current_module . '/' . $i . '">' . $v . '</a></li>';
		}
		
		$module_nav_html .= '</ul>';
	}
	
	

// (7) Get Admin Nav
// -----------------

	$admin_nav_html = '<ul class="nav">';
	$landing_nav = '';
	foreach((array)$modules as $i => $v)
	{
		if(file_exists(METEOR_MODULE_PATH . '/' . $v . '/admin.php'))
		{
			$cur = ($current_module == $v) ? ' class="active"' : '';
			$m_name = ucwords(str_replace('_', ' ', $v));
			$admin_nav_html .= '<li' . $cur . '><a href="/' . METEOR_ADMIN_ID . '/' . $v . '">' . $m_name . '</a></li>';
			$landing_nav .= '<a class="btn btn-primary btn-lg" href="/' . METEOR_ADMIN_ID . '/' . $v . '" role="button">' . $m_name . '</a> ';
		}
	}
	$admin_nav_html .= '</ul>';


	
// (8) Get template
// ----------------

	$tmp = $theme_path . '/main.php';
	$template = 'Template Not Found (' . $tmp . ')';
	if(file_exists($tmp) && is_readable($tmp))
	{
		ob_start();
		include($tmp);
		$template = ob_get_clean();
	}


// (9) Get Content
// ----------------

	foreach((array)$content as $i => $v)
	{
		if(file_exists(METEOR_CONTENT_PATH . '/' . $v))
		{
			$m_name = ucwords(str_replace('.html', ' ', $v));
			$content_nav_html .= '<li' . '><a href="/' . METEOR_ADMIN_ID . '/' . $v . '">' . $m_name . '</a></li>';
		}
	}
	

	error_log($content_nav_html);

	
// (10) Set template vars
// ---------------------
	
	// Constant vars
	$template_vars = array(
						'admin_nav' => $admin_nav_html,
						'content_nav' => $content_nav_html,
						'current_module' => $current_module,
						'module_nav' => $module_nav_html,
						'module_name' => $module_name,
						'current_controller' => $current_controller,
						'controller_nav' => $controller_nav_html,
						'controller_name' => $controller_name,
						'formatted_controller_name' => ucwords(str_replace('.', ' &raquo; ', $controller_name)),
						'theme_path' => $theme_web_path,
						'site_name' => data_get_setting('site_name'),
						'admin_web_root' => '/' . METEOR_ADMIN_ID,
						'username' => ucwords($_SESSION['meteor']['user']['username'])
						);
	
	
	
// (11) Defaults
// -------------

	if($controller == '' && $current_controller == '')
	{	
		$controller = '<div class="jumbotron">
						  <h1>Welcome Back {username}.</h1>
						  <p>Get busy work\'n. Jump into a module:</p>
						  <p>' . $landing_nav . '</p>
						</div>';
	}
	
	
	
// (x) Load and echo website
// -------------------------
	
	$website = $template;
	$website = str_replace('{controller}', $controller, $website);
	
	foreach($template_vars as $i => $v)
		$website = str_replace('{' . $i . '}', $v, $website);

	
	
	// Echo Website
	// -----------
	echo $website;
	
	
	
	
	
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* /// Functions ////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
	


	
?>