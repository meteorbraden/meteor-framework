<?php
	
	session_start();
	
	// (1) Define Globals
	// --------------------
	define("IN_DEVELOPMENT", true);
	define("METEOR_DIR", '/meteor');
	define("METEOR_PATH", $_SERVER['DOCUMENT_ROOT'] . METEOR_DIR);
	define("METEOR_FRAMEWORK_PATH", METEOR_PATH . '/framework');
	define("METEOR_CONTENT_PATH", METEOR_PATH . '/content');
	define("METEOR_MODULE_PATH", METEOR_PATH . '/modules');
	define("METEOR_MODULE_WEB_PATH", METEOR_DIR . '/modules');
	define("METEOR_THEME_PATH", METEOR_PATH . '/themes');
	define("METEOR_THEME_WEB_PATH", METEOR_DIR . '/themes');
	define("METEOR_ADMIN_ID", 'admin');
	define("METEOR_LOGIN_ID", 'admin-login');
	define("METEOR_DEPTH_ID", '_');
	define("METEOR_INVISIBLE_FILES", ".,..,.htaccess,.htpasswd,.DS_Store,_notes,.git,.svn");
	define("METEOR_URI", trim(strtok($_SERVER['REQUEST_URI'], '?'), '/'));
	
	
	// (2) Include site specific settings
	// ------------------------------------
	if(file_exists(METEOR_FRAMEWORK_PATH . '/settings.php'))
		include(METEOR_FRAMEWORK_PATH . '/settings.php');
	
		
	// (3) Connect to database and site settings
	// -------------------------------------------
	if(file_exists(METEOR_FRAMEWORK_PATH . '/data.php'))
	{
		include(METEOR_FRAMEWORK_PATH . '/data.php');
		
		// Set site settings
		if((IN_DEVELOPMENT || !isset($_SESSION['meteor']['site_settings'])) && function_exists('get_site_settings'))
		{
			$site_settings = get_site_settings();
			
			foreach($site_settings as $i => $v)
				$_SESSION['meteor']['site_settings'][$v['name']] = $v['value'];
		}
	}
	
	
	// (3) Error handling
	// --------------------
	if(IN_DEVELOPMENT)
	{
		ini_set('display_errors', 1);
		error_reporting(E_ALL);
	}
	else
		error_reporting(0);
	

	
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* /// Functions ////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */

	
	// (x) Include Module Functions
	// ----------------------------
	function include_module($module)
	{
		if(file_exists(METEOR_MODULE_PATH . '/' . $module . '/functions.php'))
			include(METEOR_MODULE_PATH . '/' . $module . '/functions.php');
		else
			echo $module . ' functions not found.';
	}
	
?>